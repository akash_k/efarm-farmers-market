#  Farmer's market

## Description

Farmer's Market is a market place where farmer's can register themselves and directly sell to the customer. Farmer can add products , update his profile, delete added product, update added product and he can also upload a video. Customer can view products, view particular farmer products, search farmer products, search a particular product, delete his account, place an order, view his orders. The system also provides various admin functionalities like to view all customers, farmers, products. He can also approve the requests, view revenue details per day, view bill details, view graphs of rides, change delivery status of order etc.

The system designed is a 3 tier application that functions as Farmer's Market place. First tier consists of Farmer's Market client that interacts with Rabbitmq which is a message middleware. Rabbitmq middleware handles many simultaneous requests. Third tier is the Farmer's Market server that receives the messages from RabbitMQ and performs CRUD operations on the third tier database. To increase performance we also implemented Redis Cache at client side, connection pooling on the server side. We used two databases mysql for storing structed data and mongo db for storing unstructured data.

## Technology Stack: 

* NodeJs 
* AngularJs 
* MongoDB
* mySQL 
* rabbit MQ
* Redis.io
 